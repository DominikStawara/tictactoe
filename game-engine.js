var newboard;
const user = 'X';
const pc = 'O';
var p = document.createElement("p");
const when_win = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[6, 4, 2]
]

const cells = document.querySelectorAll('.cell');
startGame();

function startGame() {
	p.remove();
	newboard = Array.from(Array(9).keys());
	for (var i = 0; i < cells.length; i++) {
		cells[i].innerText = '';
		cells[i].style.removeProperty('background-color');
		cells[i].addEventListener('click', turn_click, false);
	}
}

function turn_click(square) {
	if (typeof newboard[square.target.id] == 'number') {
		turn(square.target.id, user)
		if (!check_tie()) turn(best_place(), pc);
	}
}

function turn(square_id, player) {
	newboard[square_id] = player;
	document.getElementById(square_id).innerText = player;
	let game_won = check_win(newboard, player)
	if (game_won) game_over(game_won)
}

function check_win(board, player) {
	let plays = board.reduce((a, e, i) => (e === player) ? a.concat(i) : a, []);
	let game_won = null;
	for (let [index, win] of when_win.entries()) {
		if (win.every(elem => plays.indexOf(elem) > -1)) {
			game_won = {index: index, player: player};
			break;
		}
	}
	return game_won;
}

function game_over(game_won) {
	for (let index of when_win[game_won.index]) {
		document.getElementById(index).style.backgroundColor = game_won.player == user ? "#0c0" : "#c00";
	}
	for (var i = 0; i < cells.length; i++) {
		cells[i].removeEventListener('click', turn_click, false);
	}
    
	game_won.player == user ? person() : cpu()
}


function person(){
      p.innerHTML = "BRAWO! Wygrałeś!";
      p.style.fontSize = "30px";
      p.style.color= "#0c0";
      p.style.textShadow= "2px 2px #050";
      p.style.textAlign= "center";
      document.body.appendChild(p);
}

function cpu(){
      p.innerHTML = "NIESTETY! Przegrałeś!";
      p.style.fontSize = "30px";
      p.style.color= "#c00";
      p.style.textShadow= "2px 2px #500";
      p.style.textAlign= "center";
      document.body.appendChild(p);
}

function draw(){
      p.innerHTML = "REMIS!";
      p.style.fontSize = "30px";
      p.style.color= "#00c";
      p.style.textShadow= "2px 2px #005";
      p.style.textAlign= "center";
      document.body.appendChild(p);
}


function empty_square() {
	return newboard.filter(s => typeof s == 'number');
}

function best_place() {
	return empty_square()[0];
}

function check_tie() {
	if (empty_square().length == 0) {
		for (var i = 0; i < cells.length; i++) {
			cells[i].removeEventListener('click', turn_click, false);
		}
		draw();
		return true;
	}
	return false;
}